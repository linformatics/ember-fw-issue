import {validator, buildValidations} from 'ember-cp-validations';

export default buildValidations({
    title: validator('presence', {
        presence: true,
        ignoreBlank: true,
        message: 'Title must be defined'
    }),
    rawContent: validator('presence', {
        presence: true,
        ignoreBlank: true,
        message: 'Issue description must be filled'
    })
});
