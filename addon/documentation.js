/* only for documentation purposes */
/**
 * Components defined by `ember-fw-issue`.
 * @module Components
 * @main Components
 */
/**
 * The primary purpose of ember-fw-issue is to integrate Issue Tracker App into all the rest of the apps.
 * This allows users who do not have access to Issue Tracker to report issues without needing to be within
 * the app itself. This documentation is designed just to be the API docs, which can aid in developers. For a more comprehensive
 * guide to Ember FW Issue, see our [Ember FW Issue](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-issue) docs.
 * @module Introduction
 * @main Introduction
 */
