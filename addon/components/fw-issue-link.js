import Component from '@ember/component';
import {computed} from '@ember/object';
import {inject} from '@ember/service';
import {isNone} from '@ember/utils';
import {create as createIssue} from '../objects/issue';
import layout from '../templates/components/fw-issue-link';

/**
 * `FwIssueLink` is the only element of `ember-fw-issue` which actually is able to be called in apps externally to Issue Tracker.
 * It designs the "Report an Issue" link for the help dropdown, which internally calls the `report-issue` modal.
 *
 * It is pretty simple to call, taking only one parameter:
 *
 * ```handlebars
 * <FwIssueLink @fallback="http://..."/>
 * ```
 *
 * **Note**: Many apps define a `title` property for `FwIssueLink`, but this is entirely pointless, as there is no title property that exists ("Report an issue"
 * is hardcoded as part of this component).
 * @class FwIssueLink
 * @module Components
 */
export default Component.extend({
    store: inject(),
    appMeta: inject(),

    attributeBindings: ['href', 'target'],
    layout,
    tagName: 'a',

    /**
     * App metadata for the issue app. Will be null if the app is not loaded
     * @property app
     * @type {AppMeta}
     * @private
     */
    app: null,

    /**
     * Fallback link in case the regular issue tracker goes down
     * @property fallback
     * @type {String}
     */
    fallback: null,

    /**
     * Computed property. Returns the URL when clicking this link. Will be the fallback URL if issue app is not found.
     * @property href
     * @type {String}
     * @private
     */
    href: computed('app', 'fallback', function() {
        return isNone(this.get('app')) ? this.get('fallback') : null;
    }),

    /** Target parameter for fallback link. Computed property. If fallback link is set, will be `_blank`.
     *
     * @property target
     * @type {String}
     * @private
     */
    target: computed('app', 'fallback', function() {
        return isNone(this.get('app')) && this.get('fallback') ? '_blank' : null;
    }),

    didReceiveAttrs() {
        this._super(...arguments);

        this.get('appMeta').findMeta('issue').then((meta) => {
            this.set('app', meta);
        });
    },

    click() {
        if (isNone(this.get('app'))) {
            return;
        }
        this.set('issue', createIssue(this));
    }
});
