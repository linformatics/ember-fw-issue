import Component from '@ember/component';
import {computed} from '@ember/object';
import {alias, and, equal} from '@ember/object/computed';
import {inject} from '@ember/service';
import {isNone} from '@ember/utils';
import {createErrorMessage} from '@bennerinformatics/ember-fw/utils/error';
import RSVP from 'rsvp';
import layout from '../../templates/components/modals/report-issue';

/**
 * This is a modal that is designed to be used by every app to report an issue about that app within that app, rather than having to navigate to
 * issue tracker. It does not have access to Issue Trackers various Ember models because it is outside the context of the Issue Tracker App.
 * Thus the modal which is used within the Issue Tracker App itself extends this modal and adds a few features to it (such as flags). This modal
 * is called within the `FwIssueLink`, so you never need to actually call it, it is just helpful to know what it does.
 *
 * Private properties are available to be overridden in Issue Tracker App (unless they are read only), but not able to be passed into the modal via the model property.
 *
 * @class ReportIssueModal
 * @module Components
 */
/**
 * Boolean determining whether the user can set extended options. Null when uninitialized
 * @type {Boolean}
 */
let extendedOptions = null;

/**
 * Valid options for issue types for most users (Bug and Feature). Constant.
 * @property BASIC_ISSUE_TYPES
 * @type {Array}
 * @private
 * @readonly
 */
const BASIC_ISSUE_TYPES = [
    {name: 'Bug', id: 'bug'},
    {name: 'Feature', id: 'enhancement'}
];
/**
 * Valid options for issue types for admins and developers (Bug, Feature, and Proposal). Constant.
 * @property FULL_ISSUE_TYPES
 * @type {Array}
 * @private
 * @readonly
 */
const FULL_ISSUE_TYPES = [
    {name: 'Bug', id: 'bug'},
    {name: 'Feature', id: 'enhancement'},
    {name: 'Proposal', id: 'proposal'}
];

/**
 * Valid options for issue priorities (default: Low, Normal, Important, Critical, and Release ASAP). Can be overridden
 * when extending this modal in Issue Tracker App to add or change priorityOptions, but it currently is not overridden.
 * @property priorityOptions
 * @private
 * @type {Array}
 */
const priorityOptions = [
    {name: 'Low', id: 'trivial'},
    {name: 'Normal', id: 'minor'},
    {name: 'Important', id: 'major'},
    {name: 'Critical', id: 'critical'},
    {name: 'Release ASAP', id: 'blocker'}
];

export default Component.extend({
    ajax: inject(),
    config: inject(),
    currentUser: inject(),
    notifications: inject(),

    priorityOptions,

    /**
     * Valid options for the status dropdown. Computed property, which displays proper status based upon the
     * details of the issue itself. A summary of when certain statuses appear is as follows:
     *
     * * `Unconfirmed`: Appears when either user is not an admin, or if the user is outside the Issue Tracker App. Resolves to `new` in Bitbucket. Note: issues
     * with this status do not appear in the Issue Tracker App on the "Open Issues" page for users with a developer role.
     * * `Confirmed`: Appears if there is no lastPushed date. Resolves to `open` in Bitbucket.
     * * `In development`: Appears if there is a lastPushed date for the issue. Resolves to `open` in Bitbucket.
     * * `Road BLock`: Appears when there is an assignee for the issue. Resolves to `on hold` in Bitbucket.
     * * `Future`: Appears if the user is an admin when there is not an assignee for the issue. Resolves to `on hold` in Bitbucket. Note: issues with this status
     * do not appear in the Issue Tracker App on the "Open Issues" page for users with a developer role.
     * * `Ready for Release`: Appears in the dropdown only when the issue has an assignee. Resolves to `resolved` in Bitbucket.
     * * `Duplicate`: Always appears. Resolves to `duplicate` in Bitbucket.
     * * `Invalid`: Always appears. Resolves to `invalid` in Bitbucket.
     *
     * Note: The `Resolved` (Bitbucket: `closed`) issue status never shows up in this dropdown as the only way to close an issue is to releas it.
     *
     * @property statusOptions
     * @type {Computed Array}
     * @private
     */
    statusOptions: computed('issue.{assignee.id,assigned,lastPushed}', 'currentUser', function() {
        let options = [
            {name: this.get('issue.lastPushed') ? 'In development' : 'Confirmed', id: 'open'}
        ];

        if (this.get('issue.isNew') || !this.get('isIssueApp') || this.get('currentUser').match('admin')) {
            options.unshift({name: 'Unconfirmed', id: 'new'});
        }

        if (this.get('currentUser').match('admin')) {
            // if admin add Unconfirmed to the beginning
            options.pushObjects([
                {name: (this.get('issue.assigned') || this.get('issue.assignee.id')) ? 'Road Block' : 'Future', id: 'on hold'},
                {name: 'Rejected', id: 'wontfix'}
            ]);
        }
        if (this.get('currentUser').match('developer') && !this.get('currentUser').match('admin') && (this.get('issue.assigned') || this.get('issue.assignee.id'))) {
            // if developer and issue is assigned to someone, you can mark it on hold.
            options.pushObject({name: 'Road Block', id: 'on hold'});
        }

        if (this.get('issue.assigned') || this.get('issue.assignee.id')) {
            options.pushObject({name: 'Ready for release', id: 'resolved'});
        }
        options.pushObjects([
            {name: 'Duplicate', id: 'duplicate'},
            {name: 'Invalid', id: 'invalid'}
        ]);

        return options;
    }),
    layout,
    tagName: 'form',

    /**
     * The model property for the report-issue modal takes two properties: `issue`, which is the issue model that is being edited, and `fallback`, which is a string of a
     * link to be displayed as "Alternative Link" if there is an error saving the issue to Issue Tracker.
     *
     * @property model
     * @type {Hash}
     */

    /**
     * Alias of model.issue
     * @property issue
     * @type {DS.Model}
     * @private
     * @readonly
     */
    issue: alias('model.issue'),
    /**
     * Alias of model.fallback
     * @property fallback
     * @type {String}
     * @private
     * @readonly
     */
    fallback: alias('model.fallback'),

    /**
     * Options used in the type modal. Returns either FULL_ISSUE_TYPES or BASIC_ISSUE_TYPES depending on whether `extendedOptions` is true or not.
     * @property typeOptions
     * @type {Array}
     * @private
     */
    typeOptions: computed('extendedOptions', function() {
        return this.get('extendedOptions') ? FULL_ISSUE_TYPES : BASIC_ISSUE_TYPES;
    }),

    /**
     * If true, user has permissions to see additional options. Set by `didReceiveAttrs` based on user roles.
     *
     * @property extendedOptions
     * @type {Boolean}
     * @private
     */
    extendedOptions: false,

    /**
     * If true, we are in the issue app. False if any other app.
     * @property isIssueApp
     * @type {Boolean}
     * @private
     * @readonly
     */
    isIssueApp: equal('config.appId', 'issue'),

    /**
     * If true, the user is currently editing the issue. False is new issue
     * @property editing
     * @type {Boolean}
     * @private
     */
    editing: false,

    /**
     * Overridden in issue tracker. Not used in external modal.
     * @property appVersions
     * @type {Array}
     * @private
     */
    appVersions: null,
    /**
     * Overridden in issue tracker. Not used in external modal.
     * @property sortedUsers
     * @type {Array}
     * @private
     */
    sortedUsers: null,
    /**
     * Overridden in issue tracker. Not used in external modal.
     * @property sortedFlags
     * @type {Array}
     * @private
     */
    sortedFlags: null,
    /**
     * Set to true when a request errors to show the fallback link
     * @property didError
     * @type {Boolean}
     * @private
     */
    didError: false,

    /**
     * Whether or not to show the fallback link in the modal. Computed if there is an error saving and a fallback link set.
     * @property showFallback
     * @type {Boolean}
     * @private
     * @readonly
     */

    showFallback: and('didError', 'fallback'),

    /**
     * Computed property which returns the CSS class to use for dropdown field width based on the current field count.
     * @return {string} CSS class for this field count
     */
    dropdownWidth: computed('extendedOptions', function() {
        let count = 1;
        if (this.get('extendedOptions')) {
            count += 2;
        }
        if (count > 1) {
            return `col-xs-12 col-sm-${Math.round(12 / count)}`;
        }
        return 'col-xs-12';
    }),

    /** Title for the modal */
    title: computed('config.name', 'editing', 'issue.{issueId,appName}', function() {
        let app = this.get('issue.appName') || this.get('config.name') || 'App';
        if (this.get('editing')) {
            return `Edit ${app} Issue ${this.get('issue.issueId')}`;
        }
        return `Report ${app} Issue`;
    }),

    didReceiveAttrs() {
        this._super(...arguments);
        let isAdmin;
        // if we already checked, use the cached value
        if (!isNone(extendedOptions)) {
            this.set('extendedOptions', extendedOptions);
        } else if (this.get('isIssueApp')) {
            // if we are the issues app, check app roles
            extendedOptions = this.get('currentUser').match(['admin', 'developer']);
            isAdmin = this.get('currentUser').match('admin');
            this.set('extendedOptions', extendedOptions);
        } else {
            // otherwise fetch them from the issues app
            this.get('config').promiseAppUrl('issue', 'roles').then((url) => {
                return this.get('ajax').request(url);
            }).then(({roles}) => {
                extendedOptions = roles.includes('admin') || roles.includes('developer');
                isAdmin = roles.includes('admin');
                this.set('extendedOptions', extendedOptions);
            });
        }
        this.set('issue.autoConfirm', isAdmin);
    },

    /**
     * Handles rolling back an issue model if used
     *
     * @private
     * @method rollback
     */
    rollback() {
        // this is overriden in the issue app
    },

    /**
     * Helper method to determine if an issue is dirty
     *
     * @private
     * @method isDirty
     */
    isDirty() {
        // this is overriden in the issue app which uses models
        return false;
    },

    /**
     * Called by the confirm action to save the issue. Extracted to make it easier to override.
     * @private
     * @method saveIssue
     * @return {Promise}  Promise that resolves when the issue saves
     */
    saveIssue() {
        if (!this.get('issue.app')) {
            // if not set, that means it's being reported from the app
            this.set('issue.app', this.get('config.appId'));
        }
        let message = this.get('issue.isNew') ? 'reported' : 'edited';
        return RSVP.resolve(this.get('issue').save()).then(() => {
            this.get('notifications').showSuccess(`Successfully ${message} issue`, 'fixed', true);
            this.get('notifications').closeAllNotifications('modal-top');
            this.closeModal();
        });
    },

    actions: {
        confirm() {
            return this.saveIssue().catch((error) => {
                // TODO: ember validations error is currently not rethrown, so we get empty
                if (error) {
                    this.get('notifications').showError(createErrorMessage(error), 'modal-top', true);
                    this.set('didError', true);
                }
                throw error;
            });
        },

        // Overridden in Issue Tracker
        addFlag() {},
        allowNewOption() {},

        cancel() {
            // dirty models cancel immediately
            // non-dirty models confirm to close
            if (!this.isDirty() || confirm('Are you sure you want to cancel? You have unsaved changes.')) {
                this.rollback();
                this.get('notifications').closeAllNotifications('modal-top');
                this.closeModal();
            }
        },

        mutVersion(version) {
            this.set('issue.version', version.get('name'));
            this.set('selectedVersion', version);
        }
    }
});
