import {getOwner} from '@ember/application';
import EmberError from '@ember/error';
import EmberObject from '@ember/object';
import {inject} from '@ember/service';
import {isNone} from '@ember/utils';
import ValidationMixin from '@bennerinformatics/ember-fw/mixins/validation';
import IssueValidation from '@bennerinformatics/ember-fw-issue/mixins/issue-validation';
import RSVP from 'rsvp';

/**
 * List of all keys in this object, used to convert to JSON
 * @type {Array}
 */
const KEYS = ['app', 'title', 'rawContent', 'priority', 'type', 'state', 'comment'];

/**
 * This object represents an issue to save to the serverside.
 * It behaves similarly to an Ember model, but bypasses a lot of logic caches we don't need.
 * It needs to be defined in this way because in other apps, we have no access to the issue model
 * defined in the Issue Tracker app, and so this Issue EmberObject is designed to fill that role.
 * @type {EmberObject}
 */
const Issue = EmberObject.extend(IssueValidation, ValidationMixin, {
    ajax: inject(),
    config: inject(),

    /** @type {String} */
    title: null,

    /** @type {String} */
    rawContent: null,

    /** @type {String} */
    priority: 'minor',

    /** @type {String} */
    type: 'bug',

    /** @type {String} */
    status: 'new',

    /** @type {String} */
    comment: null,

    /**
     * If true, this record has already been saved, thus should not be saved again
     * @type {Boolean}
     */
    saved: false,

    /**
     * Converts this issue object to JSON to send to the server
     * @return {Object} JavaScript object ready for a REST request
     */
    toJson() {
        return {issue: this.getProperties(KEYS)};
    },

    /**
     * Logic to save an issue to the serverside
     * @return {Promise} Promise that resolves to the saved issue
     */
    save() {
        // if already saved, do not try to save again
        if (this.get('saved')) {
            return RSVP.resolve(this);
        }

        // first, check that validations pass
        return this.validate().then(() => {
            // then ensure issue metadata was fetched, most likely it was by this point but promise in case
            let config = this.get('config');
            return config.promiseAppUrl('issue', 'issues', config.get('appId'));
        }).then((url) => {
            // and finally make the request for data
            return this.get('ajax').post(url, {data: this.toJson()});
        }).then(({issue}) => {
            this.setProperties(issue);
            this.set('saved', true);
            return this;
        });
    }
});
export default Issue;

/**
 * Creates a new issue, injecting the owner required for CP validations. This can be imported like a util function and used in the code of ember-fw-issue.
 * @param  {EmberObject} parent Parent object, normally can just be this
 * @return {Issue}              Issue object
 */
export function create(parent) {
    if (isNone(parent)) {
        throw new EmberError('Missing parent parameter for Issue.create');
    }
    return Issue.create(getOwner(parent).ownerInjection());
}
